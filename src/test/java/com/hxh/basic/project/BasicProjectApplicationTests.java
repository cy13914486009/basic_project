package com.hxh.basic.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hxh.basic.project.config.WxAgentConfig;
import com.hxh.basic.project.vo.WxAgent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicProjectApplicationTests {

    @Autowired
    private WxAgentConfig wxAgentConfig;

    @Test
    public void contextLoads() {
    }

    @Test
    public void propsTest() throws JsonProcessingException {

        WxAgent ws2 =  wxAgentConfig.ofAgentId("1");
        String code = ws2.getSecret();
        System.out.println(code);

        List<WxAgent> list = wxAgentConfig.getAgents();
        for(WxAgent ws : list){
            if(ws.getAgentId().equals("1")){
                System.out.println(ws.getSecret());
            }
        }
    }

}
