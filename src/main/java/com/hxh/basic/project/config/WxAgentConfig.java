package com.hxh.basic.project.config;
import com.hxh.basic.project.vo.WxAgent;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C), 2008-2020, 国家电网公司客户服务中心信息运维中心
 * Project:  wxws
 *
 * @author: bingdor
 * Date:     2020/9/7 12:26 下午
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
@Configuration
@ConfigurationProperties(prefix = "mxps.mxapps")
public class WxAgentConfig implements InitializingBean {
    private static final Map<String, WxAgent> AGENT_MAP = new HashMap<>();
    private Map<String,String> agents;
    
    public List<WxAgent> getAgents(){
        return new ArrayList<>(AGENT_MAP.values());
    }

    public void setAgents(Map<String, String> agents) {
        this.agents = agents;
    }
    
    public WxAgent ofAgentId(String agentId){
        return AGENT_MAP.get(agentId);
    }

    @Override
    public void afterPropertiesSet() {
        if (agents != null && !agents.isEmpty()) {
            for (Map.Entry<String, String> entry : agents.entrySet()) {
                WxAgent wxAgent = new WxAgent();
                wxAgent.setAgentId(entry.getKey());
                wxAgent.setSecret(entry.getValue());
                AGENT_MAP.put(wxAgent.getAgentId(),wxAgent);
            }
        }
    }
}
