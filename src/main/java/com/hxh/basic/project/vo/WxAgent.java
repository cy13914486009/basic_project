package com.hxh.basic.project.vo;

/**
 * Copyright (C), 2008-2020, 国家电网公司客户服务中心信息运维中心
 * Project:  wxws
 *
 * @author: bingdor
 * Date:     2020/9/7 12:32 下午
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
public class WxAgent {
    private String agentId;
    private String secret;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
