package com.hxh.basic.project;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cuiyang
 * @title: myPropsConfig
 * @projectName project
 * @description: TODO
 * @date 2020/11/220:23
 */
@Component
@ConfigurationProperties(prefix="mxps") //接收application.yml中的mxps下面的属性
public class myPropsConfig {

    private Map<String, String> mapProps = new HashMap<>(); //接收mapProps里面的属性值

    public Map<String, String> getMapProps() {
        return mapProps;
    }

    public void setMapProps(Map<String, String> mapProps) {
        this.mapProps = mapProps;
    }
}
